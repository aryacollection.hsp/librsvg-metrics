FROM node:16-alpine as build-frontend

WORKDIR /app

COPY frontend/package.json frontend/package-lock.json frontend/tsconfig.json /app/
RUN npm install

COPY ./frontend/src /app/src/
COPY ./frontend/public /app/public/

RUN npm run build

FROM python:3.11.1

WORKDIR /app

COPY ./backend/requirements.txt /app
RUN pip install -r /app/requirements.txt

COPY ./backend /app
COPY --from=build-frontend /app/build /app/build

ENV FLASK_ENV production
EXPOSE 3000

CMD ["gunicorn", "-b", ":3000", "server:app"]
