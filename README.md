# librsvg-metrics

A flask web app to store metrics data for librsvg with a react frontend to render visualizations for the same.

Follow the steps in the backend/README.md to setup the backend. Then, run `npm install` in the frontend directory to
install the dependencies for the frontend. Finally, run `npm start` to start the frontend server.
