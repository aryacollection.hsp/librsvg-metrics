type Metric = {
    data: {
        type: string;
        [key: string]: number;
        revision: string;
        name?: string;
    };
    created: number;
};
