import React from 'react';
import {Line} from 'react-chartjs-2';
import 'chartjs-adapter-moment';

type BenchmarkProps = {
  name: string;
  minDate: string;
  field: string;
  metrics: Array<Metric>;
};

export function Benchmark({name, minDate, field, metrics}: BenchmarkProps) {
    const options = {
        scales: {
            x: {
                type: "time" as const,
                time: {
                    displayFormats: {
                        hour: "h:mm a",
                        day: "D MMM",
                        weeks: "D MMM yyyy"
                    },
                    unit: "day" as const,
                    tooltipFormat: "D MMM yyyy hh:mm a"
                },
                min: minDate
            }
        },
        showLine: false
    };
    const data = {
        datasets: [{
            label: `${name} benchmark`,
            data: metrics?.map(m => ({x: m.created, y: m.data[field]})),
            backgroundColor: 'rgba(255, 99, 132, 1)',
        }]
    };

    return <>
        <h2>{name}, {field}</h2>
        <Line options={options} data={data}/>
    </>;
}
