import React, {useEffect, useState} from 'react';
import './App.css';
import {Benchmark} from "./Benchmark";
import {
    Chart as ChartJS,
    Legend,
    LinearScale,
    LineElement,
    PointElement,
    TimeScale,
    Tooltip
} from "chart.js";
import moment from "moment";

ChartJS.register(LinearScale, PointElement, LineElement, Tooltip, Legend, TimeScale);


function App() {
    const names = ["hicolor-apps", "symbolic-icons"];
    const fields = ["Ir", "mem_total"];
    const benchmarks = fields.flatMap(field => (
        names.flatMap(name => ({name: name, field: field}))
    ));
    const [metrics, setMetrics] = useState<Array<Metric>>([]);
    useEffect(() => {
       fetch("/api/metrics/")
           .then(r => r.json())
           .then(x => setMetrics(x))
           .catch(err => console.log(err));
    }, []);

    const minDate = moment().subtract(30, "days").format();

    return (
        <div className="App">
            {benchmarks.map(entry => (
                <Benchmark
                    name={entry.name}
                    field={entry.field}
                    minDate={minDate}
                    key={`${entry.name}-benchmark`}
                    metrics={metrics?.filter(x => (x.data.name === entry.name))}
                />
            ))}
        </div>
    );
}

export default App;
