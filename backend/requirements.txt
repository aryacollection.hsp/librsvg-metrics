Flask==2.2.2
Flask-SQLAlchemy==3.0.2
SQLAlchemy==1.4.44
psycopg2-binary==2.9.5
gunicorn==20.1.0
