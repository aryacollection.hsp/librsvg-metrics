A flask web app to store metrics data for librsvg.

## Backend Setup

1. Setup a Python virtual environment for the project.
2. Install the dependencies using `pip install -r requirements.txt`.
3. Create `config.py` file using `config.py.sample` as an example.
4. Run a Postgres server and update `config.py` with the appropriate url to connect to it.
5. Run `flask init-db` to initialize the database.
6. Create a sample user for submitting metrics. Run `flask shell` and execute:

   ```python
   from models import User
   
   name = "YOUR_USERNAME"
   token = "YOUR_TOKEN"

   db.session.add(User(name=name, token=token))
   db.session.commit()
   ```
7. Run `flask run` to bring up the development web server.
8. Use curl or any other tool to send sample http requests.

   ```shell
   # to submit metrics
   curl http://127.0.0.1:5000/api/metrics/ \
    -H 'Authorization: Token YOUR_TOKEN' \
    -H 'Content-Type: application/json' \
    --data '{"commit": "1234abc", "time": 59, "random": false}'
   
   # to retrieve metrics
   curl http://127.0.0.1:5000/api/metrics/  -H 'Authorization: Token YOUR_TOKEN'
   ```
9. You can optionally run `flask insert-dummy-data` to insert dummy data into the database.


