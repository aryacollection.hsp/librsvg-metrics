from sqlalchemy import Identity, Integer, Text, Column, func, DateTime, ForeignKey
from sqlalchemy.dialects.postgresql import JSONB

from app import db


class User(db.Model):
    __tablename__ = "user"

    id = Column(Integer, Identity(), primary_key=True)
    name = Column(Text, nullable=False)
    comment = Column(Text)
    token = Column(Text, unique=True, nullable=False)
    created = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)


class Metric(db.Model):
    __tablename__ = "metric"

    id = Column(Integer, Identity(), primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    data = Column(JSONB)
    created = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
