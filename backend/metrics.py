from flask import request, Blueprint, jsonify

from app import db
from errors import make_error
from models import User, Metric

metrics_bp = Blueprint("metrics", __name__)


@metrics_bp.post('/metrics/')
def submit_metric():
    """ Submit metrics to database as an authenticated user. """
    auth_header = request.headers.get("Authorization")
    if auth_header is None:
        return make_error(401, "Missing Authorization header.")

    try:
        token = auth_header.split(" ")[1]
    except KeyError:
        return make_error(401, "Invalid Authorization header.")

    user = db \
        .session \
        .query(User) \
        .where(User.token == token) \
        .first()

    if user is None:
        return make_error(401, "Invalid token.")

    metric = Metric(user_id=user.id, data=request.json)
    db.session.add(metric)
    db.session.commit()

    return "", 201


@metrics_bp.get('/metrics/')
def retrieve_metrics():
    """ Retrieve all metrics from the database. """
    results = db \
        .session \
        .query(Metric) \
        .order_by(Metric.created) \
        .all()
    return jsonify([
        {
            "data": r.data,
            "created": int(r.created.timestamp() * 1000)
        }
        for r in results
    ])
