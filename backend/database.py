import json
from datetime import datetime

import click
from flask.cli import with_appcontext

from app import db


@click.command()
@with_appcontext
def init_db():
    """ Clear existing data and create new tables. """
    db.drop_all()
    db.create_all()
    click.echo("Initialized the database.")


@click.command()
@with_appcontext
def insert_dummy_data():
    from models import Metric
    with open("dummy.json") as f:
        data = json.load(f)
    metrics = [
        Metric(
            user_id=1,
            created=datetime.fromtimestamp(m["push_timestamp"]),
            data={"revision": m["revision"], "value": m["value"], "type": "memory"}
        )
        for m in data
    ]
    db.session.add_all(metrics)
    db.session.commit()
