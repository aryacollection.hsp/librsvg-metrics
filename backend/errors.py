from flask import jsonify


def make_error(status, message):
    response = jsonify({"status": status, "message": message})
    response.status_code = status
    return response
