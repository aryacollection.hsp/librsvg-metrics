from flask_sqlalchemy import SQLAlchemy
from flask import Flask

API_PREFIX = "/api"
db = SQLAlchemy()


def create_app():
    app = Flask(__name__, static_folder="./build", static_url_path="/")
    app.config.from_pyfile("config.py")

    db.init_app(app)

    from database import init_db, insert_dummy_data
    app.cli.add_command(init_db)
    app.cli.add_command(insert_dummy_data)

    from views import main_bp
    app.register_blueprint(main_bp)

    from metrics import metrics_bp
    app.register_blueprint(metrics_bp, url_prefix=API_PREFIX)

    return app
